#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct Map
{
	char symbol;
	int number;
	static const int count = 26;
};

Map* createMap();
void writeMapToFile(string, Map*);
void displayContentFileWithMap(string);
void encoding(string, string, Map*);
void decoding(string, string, Map*);
Map* createDectionary(string);
int encoding(char, Map*);
char decoding(int, Map*);

int main()
{
	Map* map = createMap();
	writeMapToFile("codes", map);
	displayContentFileWithMap("codes");
	encoding("input.txt", "result.txt", map);
	decoding("result.txt", "output.txt", map);
}

Map* createMap()
{
	Map* mapArray = new Map[Map::count];

	char symbol = 'A';
	for (int i = 0; i < Map::count; i++)
	{
		mapArray[i].symbol = symbol;
		mapArray[i].number = Map::count - i;
		symbol++;
	}

	return mapArray;
}

void writeMapToFile(string fileName, Map* map)
{
	ofstream out(fileName, ios::out | ios::binary);
	for (size_t i = 0; i < Map::count; i++)
	{
		out.write((char*)&map[i], sizeof(Map));
	}
	out.close();
}

void displayContentFileWithMap(string fileName)
{
	Map* map = new Map[Map::count];
	ifstream in(fileName, ios::in | ios::binary);
	for (size_t i = 0; i < Map::count; i++)
	{
		in.read((char*)&map[i], sizeof(Map));
		cout << map[i].symbol << " " << map[i].number << endl;
	}
	in.close();
}

// As I understand, this function is to load the Map from file.
// But for what is it needed, if main function has its map and 
// passes it as a parameter to encoding and decoding functions?
// May be I wrong understand the aim of 'createDectionary', but I tried 
// to understand it so long.
Map* createDectionary(string fileName) 
{
	Map* map = new Map[Map::count];
	ifstream in(fileName, ios::in | ios::binary);
	for (size_t i = 0; i < Map::count; i++)
	{
		in.read((char*)&map[i], sizeof(Map));
	}
	in.close();
	return map;
}

int encoding(char symbol, Map* map)
{
	symbol = toupper(symbol);
	for (size_t i = 0; i < Map::count; i++)
	{
		if (symbol == map[i].symbol)
		{
			return map[i].number;
		}
	}
	return symbol;
}

void encoding(string input, string output, Map* map)
{
	ifstream in(input);
	ofstream out(output, ios::out | ios::binary); // write in file
	string source = "";

	while(getline(in, source))
	{
		for (int i = 0; i < source.length(); i++)
		{
			int code = encoding(source[i], map);
			out.write((char*)&code, sizeof(int));
		}
	}
	in.close();
	out.close();

}
// In decoding function realization were written 'Map const*'
// but in the prototype without 'const'. In encoding function 
// there is nowhere a const too. So I counted it a typo and deleted

void decoding(string input, string output, Map* map)
{
	ifstream in(input, ios::in | ios::binary);
	ofstream out(output, ios::out | ios::binary); // write in file
	int source = -1;
	in.read((char*)&source, sizeof(int));

	while (!in.eof())
	{
		char letter = decoding(int(source), map);
		out.write((char*)&letter, sizeof(letter));
		in.read((char*)&source, sizeof(int));
		cout << source << " ";
	}
	in.close();
	out.close();
}

char decoding(int item, Map* map)
{
	for (int i = 0; i < Map::count; i++)
	{
		if (item == map[i].number)
		{
			return map[i].symbol;
		}
	}
	return char(item);
}