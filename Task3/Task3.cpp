#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int binaryToDecimal(string);
int calculateSum(string);
int calculateSumFromFile(string);
void calculateSumTest(string);


int main()
{
	calculateSumTest("test.txt");
}

int calculateSum(string source)
{
	int sum = 0;
	int start = -1;
	for (int i = 0; i < source.length(); i++) {
		if (source[i] == '0' || source[i] == '1') {
			if (start == -1) {
				start = i;
			}
		}
		if (start != -1 && source[i + 1] != '0' && source[i + 1] != '1') {
			string binaryNumber = source.substr(start, i - start + 1);
			sum += binaryToDecimal(binaryNumber);
			start = -1;
		}
	}
	return sum;
}

int calculateSumFromFile(string fileName)
{
	ifstream in(fileName);
	string source = "";
	int sum = 0;
	while (getline(in, source))
	{
		sum += calculateSum(source);
	}
	in.close();
	return sum;
}

int binaryToDecimal(string binary)
{
	int decimalValue = 0;

	for (int i = 0; i < binary.length(); i++) {
		if (binary[i] == '1') {
			decimalValue += std::pow(2, binary.length() - 1 - i);
		}
	}
	return decimalValue;
}

void calculateSumTest(string fileName)
{
	bool result = calculateSum("1+-0100+*** 1000") == 13;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1000001+-1000+* 100** 1--- 0000001") == 79;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("10000000000011111+11+*** 1111000111") == 66537;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSum("1111111111111000011111+-11111111111+*   1111** -22--- ") == 4195885;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
	result = calculateSumFromFile(fileName) == 4262515;
	cout << "Test for parsing " << (result ? "Passed." : "Failed.") << endl;
}